 
import struct, random

def _rint():
    return random.randint(0, 255)

def hex_to_rgb(hex):
    hex = hex.lstrip('#')
    return struct.unpack('BBB', bytes.fromhex(hex))

def rgb_to_hex(r):
    return ('#%02X%02X%02X' % (r[0], r[1], r[2]))

def random_hex():
    return ('#%02X%02X%02X' % (_rint(),_rint(),_rint()))

def random_rgb():
    return (_rint(), _rint(), _rint(), 255)