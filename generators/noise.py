 
#!/usr/bin/python
from PIL import Image, ImageDraw
import click
import numpy as np
from utils import color

def generate(image, options):
    w, h = image.size

    bs = 4
    if "block-size" in options:
        bs = int(options["block-size"])

    draw = ImageDraw.Draw(image)
    for y in range(int(h/bs)):
        for x in range(int(w/bs)):
            if x+y%2 == 0:
                c = (0, 0, 0)
            else:
                c = (255, 255, 0)
            c = color.random_rgb()
            p1 = (x*bs, y*bs)
            p2 = (bs*x+bs, bs*y+bs)
            draw.rectangle([p1, p2], outline=c, fill=c)

    return image