 
#!/usr/bin/python
from PIL import Image, ImageDraw
import click
import numpy as np
from utils import color_util, util
import random

def random_close(color):
    total_distance = 15
    current_distance = 0
    color = list(color)
    while(current_distance <= total_distance):
        current = random.randint(4, 10) * random.choice([-1, 1])
        current = util.clamp(total_distance - current_distance, current, current_distance)

        index = random.randint(0, 2)

        tentative = color[index] + current
        if tentative < 0:
            tentative -= current
        elif tentative > 255:
            tentative += current
        color[index] = tentative

        current_distance += abs(current)

    return tuple(color)

def generate(image, options):
    w, h = image.size

    bs = 4
    random_colors = 10
    if "block-size" in options:
        bs = int(options["block-size"])

    if "color" in options:
        if options["color"] == "random":
            color = color_util.random_rgb() 
        else:
            color = color_util.hex_to_rgb(options["color"])
        colors = []
        for i in range(random_colors):
            colors.append(random_close(color))

    else:
        colors = [color_util.random_rgb() for x in range(random_colors)]

    #for col in colors:
        #print(color_util.rgb_to_hex(col))

    draw = ImageDraw.Draw(image)
    for y in range(int(h/bs)):
        for x in range(int(w/bs)):
            c = random.choice(colors)
            p1 = (x*bs, y*bs)
            p2 = (bs*x+bs, bs*y+bs)
            draw.rectangle([p1, p2], outline=c, fill=c)

    return image