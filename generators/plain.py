 
#!/usr/bin/python
from PIL import Image, ImageDraw
import click
import numpy as np
from utils import color_util

def generate(image, options):
    w, h = image.size

    if "color" in options:
        c = color_util.hex_to_rgb(options["color"])
        c += (255,)
    else:
        c = color_util.random_rgb()

    print(f"Generating with color {c}")
    for y in range(h):
        for x in range(w):
            image.putpixel((x, y), c)

    return image