#!/usr/bin/python
from PIL import Image, ImageDraw
import click, importlib

from generators import plain

@click.command()
@click.option("-s", "--size", help="size wXh", default="1920x1080")
@click.option("-o", "--out-filename", default="output.png")
@click.option("-g", "--generator", default="plain")
@click.option("-p", "--param", multiple=True)
def main(size, out_filename, generator, param):
    w, h = size.split("x")
    w = int(w)
    h = int(h)

    print(f"Generating a wallpaper with size {w}x{h}")

    img = Image.new('RGB', (w, h), color=(0, 0, 0))

    options = {}
    for par in param:
        k, v = par.split("=")
        options[k] = v
    if len(options) > 0:
        print("Called with the following extra parameters:")
        for k, v in options.items():
            print(f"  {k}:{v}")

    gen = importlib.import_module(f"generators.{generator}")
    img = gen.generate(img, options)

    img.save(out_filename)


if __name__ == '__main__':
    main()